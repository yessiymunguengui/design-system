# Design System

# Sujet #1


## Objectif

Un design system permet, entre autres choses, de réduire la quantité de manipulation à effectuer lors de l'évolution du design d'une application. Certains composants de l'application ont des propriétés en commun avec le reste des composants de l'application telles que les polices, couleurs, sous-composants, etc.

Regrouper et définir ces elements de base permet de gagner un temps certains lors du development d'application.

Une liste de composants est disponible à l'adresse suivante : XXXX. Implementez-les en utilisant un des frameworks
front parmi la liste suivante :

1. `Angular`

2. `Vue.js`

3. `React`

Et rendez-les visibles et personnalisables en utilisant une librairie telle que storybook. Une méthodologie telle que l'atomique design est attendue pour l'implémentation de vos composants.


Utiliser l'atomique design comme méthodologie pour l'implémentation de vos composants permet de créer un système de conception cohérent et évolutif,
qui facilite la collaboration et améliore la réutilisabilité des composants.



## Contexte & motivation derrière le kata

Un développeur front end / fullstack doit être capable de retranscrire une maquette complexe sous forme de composant
élémentaire réutilisable afin d'améliorer la maintenance et le développement futur de l'application
En tant que développeur front end / fullstack, il est important de pouvoir anticiper les améliorations probables des
différents composants à implementer pour permettre d'aborder leurs développements de la manière la plus optimale



# Specification [RFC2119](https://microformats.org/wiki/rfc-2119-fr) du kata


**1. Fonctionnalité du projet**

* Le project `DOIT` utiliser un framework actuel.

* L'ensemble des composants du design system fourni `DOIT` être implémentés et visibles grâce à une librairie telle que storybook.

* Le projet `DOIT` pouvoir être ouvert sur n'importe quel navigateur

**2. Démonstration du frontend craftsmanship**

* L'ensemble des composants du design system fourni `PEUVENT` être personnalisé facilement grâce à une librairie telle que storybook.

* Un grand pouvoir implique de grandes responsabilités. Vos choix `DOIVENT` être justifiés dans un readme.

* Le project `DEVRAIT` utiliser un framework de la liste : `Angular`, `Vue.js`,  `React`.

* Le projet `PEUT` être implémenté en Typescript.

* Le code `DEVRAIENT` être facilement lisibles et agréables à l'œil.

* La structure du projet `DOIT` être réfléchie




---
